# CI-CD pattern embracing Terraform, Google Kubernetes Engine, GitLab CI, Traefik

## Intro

Following instructions you will:
* Provision GKE cluster using terraform;
* Create gitlab-admin service account in provisioned cluster;
* Configure GitLab CI->GKE integration: add cluster IP and k8s service account token as a CI/CD variables in GitLab project;
* Build docker image for GO app. Push Docker image to DockerHub;
* Deploy k8s manifests with GO app based on git branch (version 1.0 for master, version 2.0 for beta);
* Canary deployment for app beta version (version 2.0, git branch beta). Traffic is splitted by Traefik in proportion: 90% master version, 10% beta version.



## Prerequisites

* Installed **terraform**. However, it's possible to run GitLab CI/CD Pipeline remotely (Vault implementetion will come soon).
* Installed **gcloud** cli.
* Installed **kubectl** cli.
* **GitLab** repo for k8s app and GitLab **Access key**.
* Created **Google Project**. Created **serviceaccount** and **json key** for it.

## Provision GKE

**IaC tool**: Terraform v0.12.3

**IaC repo:** [TF-GKE](https://gitlab.com/ikarlashov/tf-gk)

### Preparation

1. You need to customize your **project.tfvars** for your needs. File **kpn.tfvars** is used in this sample case.
2. Put your gcp-key.json file in repo folder (preferably). Filename is in .gitignore thus not versioned.
3. Create **secrets.tfvars** file with variable **gitlab_access_token** declaration. Filename is in .gitignore thus not versioned.

PS. Step 2 and 3 won't be required as soon as **vault** is implemented.

### How-To Provision


1. Clone [repo](https://gitlab.com/ikarlashov/tf-gk): 

```
git clone git@gitlab.com:ikarlashov/tf-gke.git
```

2. Run terraform: 

```
terraform init
terraform plan -var-file=kpn.tfvars -var-file=secrets.tfvars
terraform apply -var-file=kpn.tfvars -var-file=secrets.tfvars
```

3. Your GKE cluster is provisioned. You'll get K8S API URL as a terraform output.

## How-To connect to K8S cluster

1. Connect your acc with gcloud:

```
gcloud init
```

2. Set current project for gcloud:

```
gcloud config set project test-ci-cd-245622
```

3. Generate local ~/.kube/config file using gcloud:

```
gcloud container clusters get-credentials kpn-test --region=europe-west4-a
```

## Prerequsites for K8S

**Traefik** is used as an ingress-controller.

To install traefik into your k8s cluster:

```
kubectl apply -f kubernetes/cluster-system/traefik/traefik-rbac.yaml
kubectl apply -f kubernetes/cluster-system/traefik/traefik-ds.yaml
```

## CI/CD for sample K8S App

Source Repo: [k8s-app](https://gitlab.com/ikarlashov/k8s-app)

Go app is used as a sample application. Gitlab CI Pipeline described in **.gitlab-ci.yml**. 

To trigger deployment, go to **[Pipelines](https://gitlab.com/ikarlashov/k8s-app/pipelines)->Run Pipeline->Run For(master or beta)->Run Pipeline**

## Obtain app IP

```
kubectl get service/traeffic-lb-service -n kube-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
```
